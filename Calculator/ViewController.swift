//
//  ViewController.swift
//  Calculator
//
//  Created by MacUser on 07/04/15.
//  Copyright (c) 2015 Improve Education. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var dotButton: UIButton!
    @IBOutlet weak var history: UILabel!
    
    var userIsInTheMiddleOfTypingANumber: Bool = false
    
    var brain = CalculatorBrain()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        dotButton.setTitle(brain.numberFormatter().decimalSeparator, forState:UIControlState.Normal)
        clear()
    }
    
//    @IBAction func dot(sender: UIButton) {
//        if (display.text!.rangeOfString(formatter.decimalSeparator!) == nil) {
//            userIsInTheMiddleOfTypingANumber = true
//            appendDigit(sender)
//        }
//    }
    @IBAction func backSpace(sender: AnyObject) {
        if userIsInTheMiddleOfTypingANumber {
            if count(display.text!) > 1 {
                display.text = dropLast(display.text!)
            } else {
                displayValue = nil
            }
        }
    }
    
    @IBAction func plusMinus(sender: UIButton) {
        if userIsInTheMiddleOfTypingANumber {
            if display.text!.rangeOfString("-") != nil {
                display.text = dropFirst(display.text!)
            } else {
                display.text = "-" + display.text!
            }
        } else {
            operate(sender)
        }
    }
    
    @IBAction func appendDigit(sender: UIButton) {
        let digit = sender.currentTitle!
        if history.text!.rangeOfString("?") != nil {
            clear()
        }
        if (digit == brain.numberFormatter().decimalSeparator && (display.text!.rangeOfString(brain.numberFormatter().decimalSeparator!) != nil || display.text! == "")){
            return
        }
        if (digit == "0" && ((display.text == "0") || (display.text == "-0" )) ) {
            return
        }
        if userIsInTheMiddleOfTypingANumber {
            display.text = display.text! + digit
        } else {
            display.text = digit
            userIsInTheMiddleOfTypingANumber = true
        }
        
        
        
    }

    
    @IBAction func operate(sender: UIButton) {
        if userIsInTheMiddleOfTypingANumber {
            enter()
        }
        if let operation = sender.currentTitle {
            if let result = brain.performOperation(operation) {
                displayValue = result
                history.text =  history.text! + " ="
            } else {
                displayValue = nil
//                history.text =  history.text! + " ERROR ="
            }
        }
    }
    
    @IBAction func clear() {
        displayValue = nil
        history.text = " "
        brain.clear()
    }
    
    @IBAction func enter() {
        !
        userIsInTheMiddleOfTypingANumber = false
        if let value = displayValue {
            displayValue = brain.pushOperand(value)
        } else {
            displayValue = nil
        }
    }
    
    var displayValue: Double? {
        get {
            return brain.numberFormatter().numberFromString(display.text!)?.doubleValue
        }
        set {
            if (newValue != nil) {
//                display.text = "\(newValue!)"
                display.text = brain.numberFormatter().stringFromNumber(newValue!)
            } else {
                display.text = ""
            }
            userIsInTheMiddleOfTypingANumber = false
//            ->model
//            if !brain.displayHistory()!.isEmpty {
//                history.text = brain.displayHistory()! + " ="
//            } else {
//                history.text = nil
//            }
             history.text = brain.description() ?? ""
        }
    }
    
    @IBAction func setVariable(sender: UIButton) {
        userIsInTheMiddleOfTypingANumber = false
        let symbol = dropFirst(sender.currentTitle!)
        if let value = displayValue {
            brain.setVariable(symbol, value: value)
            displayValue = brain.evaluate()
        }
    }
    @IBAction func pushVariable(sender: UIButton) {
        if userIsInTheMiddleOfTypingANumber {
            enter()
        }
        displayValue = brain.pushOperand(sender.currentTitle!)
    }
}

